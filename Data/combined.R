library(tidyverse)
library(plyr)
library(stringr)
library(stringi)
library(Rfast)
options(scipen=999)
makaann<-read.csv("Data/2makaan1.csv")
magicbricks<-read.csv("Data/2magicbricks.csv")
acres99<-read.csv("Data/2acres99.csv")
sulekha<-read.csv("Data/1sulekha1.csv")
proptiger<-read.csv("Data/2proptiger.csv")
commonfloor<-read.csv("Data/commonFloor_final2.csv")

mb2<-str_split_fixed(commonfloor$BHK,"for",2)%>%as.data.frame()
mb1<-mb2$V1%>%as.character%>%as.data.frame()
d<-str_split_fixed(stri_reverse(commonfloor$BHK),"KHB",2)
d<-as.data.frame(d)
d1<-str_split_fixed(d$V2," ",2)%>%as.data.frame()
d$V1<-stri_reverse(d$V1)%>%as.factor()
d$V2<-stri_reverse(d1$V1)%>%as.factor()
d<-as.data.frame(d)
commonfloor$BHK<-d$V2
commonfloor$type<-d$V1

makaann<-select(makaann,lat,lon,name,address,area,price,rate,url,type,bhk)
magicbricks<-select(magicbricks,lat,lon,name,address,area,price,rate,link,type,bhk)
acres99<-select(acres99,lat,lon,name,address,area,price,rate,url,type,bhk)
proptiger<-select(proptiger,lat,lon,name,address,area,price,rate,url,type,bhk)
sulekha<-select(sulekha,Latitude,Longitude,Name,Address,Area,Price,Rate,Type,BHK..)
commonfloor<-select(commonfloor,Latitude,Longitude,Name,Address,Area,Price,Rate,type,BHK)

names(makaann)<-c("lat","lon","name","address","area","price","rate","url","type","bhk")
names(magicbricks)<-c("lat","lon","name","address","area","price","rate","url","type","bhk")
names(acres99)<-c("lat","lon","name","address","area","price","rate","url","type","bhk")
names(proptiger)<-c("lat","lon","name","address","area","price","rate","url","type","bhk")
names(sulekha)<-c("lat","lon","name","address","area","price","rate","type","bhk")
names(commonfloor)<-c("lat","lon","name","address","area","price","rate","type","bhk")

makaann<-cbind(makaann,source="makaan")
magicbricks<-cbind(magicbricks,source="magicbricks")
acres99<-cbind(acres99,source="99acres")
proptiger<-cbind(proptiger,source="proptiger")
sulekha<-cbind(sulekha,source="sulekha")
commonfloor<-cbind(commonfloor,source="commonfloor")

makaann$price<-makaann$price%>%as.factor()
magicbricks$price<-magicbricks$price%>%as.factor()
acres99$price<-acres99$price%>%as.factor()
proptiger$price<-proptiger$price%>%as.factor()
sulekha$price<-sulekha$price%>%as.factor()
commonfloor$price<-commonfloor$price%>%as.factor()

makaann$lat<-makaann$lat%>%as.character()%>%as.numeric()
magicbricks$lat<-magicbricks$lat%>%as.character()%>%as.numeric()
acres99$lat<-acres99$lat%>%as.character()%>%as.numeric()
proptiger$lat<-proptiger$lat%>%as.character()%>%as.numeric()
sulekha$lat<-sulekha$lat%>%as.character()%>%as.numeric()
commonfloor$lat<-commonfloor$lat%>%as.character()%>%as.numeric()

makaann$lon<-makaann$lon%>%as.character()%>%as.numeric()
magicbricks$lon<-magicbricks$lon%>%as.character()%>%as.numeric()
acres99$lon<-acres99$lon%>%as.character()%>%as.numeric()
proptiger$lon<-proptiger$lon%>%as.character()%>%as.numeric()
sulekha$lon<-sulekha$lon%>%as.character()%>%as.numeric()
commonfloor$lon<-commonfloor$lon%>%as.character()%>%as.numeric()





data<-plyr::rbind.fill(makaann,magicbricks,proptiger,commonfloor,acres99,sulekha)
write.csv(data,"Data/combined.csv",row.names=F)





