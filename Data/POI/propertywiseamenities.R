library(rvest)
library(jsonlite)
library(tidyverse)
library(plyr)
library(stringr)

df<-read.csv("Data/amenitieslist.csv")
d2<-read.csv("Data/selectedamenities.csv")
d2<-select(d2,-X)
df6<-data.frame()
i=1
e1<-read.csv("Data/page.csv")
e<-e1[1,1]+1
for(i in e:nrow(df)){

lat<-df[i,1]
lon<-df[i,2]
dist<-5
  
url<-paste("https://www.makaan.com/apis/getAmenities?latitude=",lat,"&longitude=",lon,"&distance=",dist,"&start=0&rows=2147483647&format=json",sep="")
data<-fromJSON(url)


df5<-cbind(lat,lon)%>%as.data.frame()
j=1
for(j in 1:nrow(d2)){
  amenity<-d2[j,1]%>%as.character
  distances<-data[[amenity]][["geoDistance"]]%>%as.data.frame()
  distances$.<-sort(distances$.)
  distances<-distances%>%as.data.frame()
  
  for (k in 1:3){
    d1<-data.frame()
    dist2<-2*k-1
    if(nrow(distances)==0){dt<-0}
    else{
    dt<-sum(distances$.<=dist2)  
    }  
    d1<-rbind(d1,dt)
    names(d1)<-paste(amenity,dist2,sep="")
    df5<-cbind(df5,d1)
  }
  
  
  
}

df6<-rbind(df6,df5)

if(i==1){write.table(df5,"Data/propertywiseamen.csv",sep=',',col.names = T,row.names=F,append=F)}
else{
  write.table(df5,"Data/propertywiseamen.csv",sep=',',col.names = F,row.names=F,append=T)
}

write.csv(i,"Data/page.csv",row.names = F)

print(paste(i))
}
    