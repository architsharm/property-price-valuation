library(tidyverse)
library(httr)
library(jsonlite)
urlauth<-"https://outpost.mapmyindia.com/api/security/oauth/token"
urlnear<-"https://atlas.mapmyindia.com/api/places/nearby/json?"


# change cid and csecret below with your own credentials
gt<-"client_credentials"
cid<-"Hv5HkUeFNizb_5uHK19GtA5gt3Nr2ytabDO0qG7HtgasGUywLaKQu5lvDmrvKnos"
csecret<-"dDJUN2FR90AMws1Htewz64WRl9tSKEQsL0S9Fee46byHKtEwQqu33PvAz0JuCsQBAqDiXyBjjrc="
CT<-"application/x-www-form- urlencoded"

keys<-"coffee"
Location<- "28.454,77.435"
token_type<-"Bearer"
access_token<-"e79520fa-c3e8-4333-ab7e-7c6e13af72b9"


results<-POST(urlauth,query=list(grant_type=gt,client_id=cid,client_secret=csecret),add_headers(Content_Type = CT))
parsed<-fromJSON(content(results,"text"),simplifyVector = FALSE)
token_type<-parsed$token_type
access_token<-parsed$access_token


final<-GET(urlnear,query=list(keywords=keys,refLocation=Location),add_headers(Authorization=paste(token_type,access_token)))
parsedfinal<-fromJSON(content(final,"text"),simplifyVector = TRUE)
Response<-parsedfinal$suggestedLocations
response<-select(Response,-keywords)


write.csv(response,"final.csv",col.names = TRUE,sep=",")
