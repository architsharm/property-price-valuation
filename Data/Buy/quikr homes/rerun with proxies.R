library(rvest)
library(tidyverse)  
library(httr)
library(stringr)
library(rlist)
library(jsonlite)

proxyurl1<-"https://free-proxy-list.net/"
proxyurl2<-"https://www.socks-proxy.net/"
proxyurl3<-"https://www.us-proxy.org/"
proxyurl4<-"https://www.sslproxies.org/"

web1<-read_html(proxyurl1)%>%html_table(fill=TRUE)
web1<-web1[[1]]%>%as.data.frame()
w1<-select(web1,`IP Address`,Port,`Last Checked`)

web2<-read_html(proxyurl2)%>%html_table(fill=TRUE)
web2<-web2[[1]]%>%as.data.frame()
w2<-select(web2,`IP Address`,Port,`Last Checked`)
web3<-read_html(proxyurl3)%>%html_table(fill=TRUE)
web3<-web3[[1]]%>%as.data.frame()
w3<-select(web3,`IP Address`,Port,`Last Checked`)
web4<-read_html(proxyurl4)%>%html_table(fill=TRUE)
web4<-web4[[1]]%>%as.data.frame()
w4<-select(web4,`IP Address`,Port,`Last Checked`)
Webproxy<-rbind(w1,w2,w3,w4)
proxytable<-Webproxy
#proxytable<-w1

useragent<-read.delim('useragents.txt',header = F)
useragent<-as.data.frame(useragent)


r <- NULL
attempt1 <- 0


while( is.null(r) &&  attempt1<=1000) {
  attempt1 <- attempt1 + 1
  attempt<-0
  while(is.null(r) && attempt < nrow(proxytable)){
  attempt<-attempt+1
  try(
    {
    ipaddress=proxytable[attempt,1]
    port=proxytable[attempt,2]
    headers = c(as.character(useragent[attempt1,1]))
    r <- source('Data/Buy/quikr homes/residential buy.R', echo=TRUE)
    }
  )
    
  }
} 
