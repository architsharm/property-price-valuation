library(stringr)

path="Data/Buy/Makaan/"
recur=F
head=T

test<-list.files(path,pattern="*.csv$")
test<-subset(test,test!=("cities.csv"))
test<-subset(test,test!=("citycenters.csv"))

for(i in 1:length(test)){

  df<-read.csv(test[i])
df<-select(df,-X)
#rate<-df$pricerate

cc<-str_split_fixed(df$pricerate,"/",2)
cc<-as.data.frame(cc)
colnames(cc)<-c("rate","unit")
#class(cc)
df<-cbind(df,cc)
df<-select(df,-pricerate,-NumPrice,-price)
df$unit<-as.character(df$unit)
df$unit<-gsub(" ","",df$unit)
df$rate<-gsub(",","",df$rate)
df<-subset(df,df$unit=="sqft")
rate<-as.numeric(df$rate)
price2<-ifelse(df$priceunit ==' L',100000,10000000)
area<-as.numeric(df$area)
price<-(round(rate*area/price2*100))/100
numprice<-(rate*area)
price<-as.data.frame(price)
numprice<-as.data.frame(numprice)
df<-cbind(df,price,numprice)

write.csv(df,paste(path,test[i],sep="")%>%as.character())
print(test[i])
}
