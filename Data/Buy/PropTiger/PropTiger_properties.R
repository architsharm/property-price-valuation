library(rvest)
library(plyr)
library(jsonlite)
library(dplyr)
library(anytime)
url_1 <- 'https://www.proptiger.com/data/v1/entity/project/'
url_2 <- '/phase'
cities <- read.csv('Data/Buy/PropTiger/cities.csv')
i <- 1
while(i <=nrow(cities)){
  city <- cities$label[i]
  projects <- read.csv(paste("Data/Buy/PropTiger/Projects/",city,".csv",sep = ''))
  df <- data.frame()
  while(j <=nrow(projects)){
    id <- projects$projectId[j]
    url <- paste(url_1,id,url_2,sep = '')
    r <- http_status(GET(url))
if(r[["category"]]!='Success'){
  next
}
    download_html(url,'test.json')
    data <- fromJSON(txt = 'test.json')
    data <- data[["data"]][["properties"]] 
    for(k in 1:length(data)){
      if(length(data[[k]])==0){
        next
      }
      temp <- cbind(data[[k]],projects$latitude[j],projects$longitude[j])
      if(!is.null(temp$imageTypeCount)){
        temp <- select(temp,-imageTypeCount) 
      }
      if(!is.null(temp$reraProject)){
        temp <- select(temp,-reraProject)
      }
      df <- rbind.fill(df,as.data.frame(temp))
    }
    print(paste(j," - ",nrow(projects),"in",city))
    j <- j+1
  }
  df <- as.data.frame(lapply(df[,], as.character))
  if(!is.null(df$createdAt) && !is.null(df$updatedAt)){
    createdAt <- anytime(as.numeric(as.character(df$createdAt))/1000) %>% as.data.frame()
    names(createdAt) <- list("createdAt")
    updatedAt <- anytime(as.numeric(as.character(df$updatedAt))/1000) %>% as.data.frame()
    names(updatedAt) <- list("updatedAt")
    df <- select(df,-createdAt,-updatedAt)
    df <- cbind(df,createdAt,updatedAt)
  }
  i <- i+1
  write.csv(df,paste("Data/Buy/PropTiger/Properties/",city,".csv",sep = '',row.names=F))
}
