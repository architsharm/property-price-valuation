# Property-Price-Valuation
A Property Price Valuation tool.

Model to estimate the property rates(per sqft) of any place in India.

Data is gathered from various property listing websites including Magicbricks,Proptiger,Makaan,99Acres.

Xgboost Regression model is used for the price prediction.

The UI is developed on Shiny in R while the model and data crawler is developed in R.


Working model -> https://gauravg36.shinyapps.io/Predictor/
